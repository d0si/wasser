package si.d0.wasser;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class WasserApplication extends Application {
    public static boolean prijavljen = false;
    public static String ime = "";
    public static String priimek = "";
    public static String tipCloveka = "";
    public static int teza = 0;
    public static boolean aktiven = false;

    public static int kolicinaPopiteVode = 0;
    public static int potrebnoPopitiVode = 0;

    private static SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        prijavljen = preferences.getBoolean("prijavljen", false);
        ime = preferences.getString("ime", "");
        priimek = preferences.getString("priimek", "");
        tipCloveka = preferences.getString("tipCloveka", "");
        teza = preferences.getInt("teza", 0);
        aktiven = preferences.getBoolean("aktiven", false);
        kolicinaPopiteVode = preferences.getInt("kolicinaPopiteVode", 0);
        potrebnoPopitiVode = preferences.getInt("potrebnoPopitiVode", 0);
    }

    public static void shrani() {
        preferences.edit()
                .putBoolean("prijavljen", prijavljen)
                .putString("ime", ime)
                .putString("priimek", priimek)
                .putString("tipCloveka", tipCloveka)
                .putInt("teza", teza)
                .putBoolean("aktiven", aktiven)
                .putInt("kolicinaPopiteVode", kolicinaPopiteVode)
                .putInt("potrebnoPopitiVode", potrebnoPopitiVode)
                .apply();
    }
}
