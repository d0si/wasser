package si.d0.wasser;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class SetupNameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_name);

        final Spinner tipCloveka = (Spinner) findViewById(R.id.spinnerTipCloveka);
        final EditText ime = (EditText) findViewById(R.id.editTextFirstname);
        final EditText priimek = (EditText) findViewById(R.id.editTextLastname);

        Button naprej = (Button)findViewById(R.id.setupNameNextButton);
        naprej.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean idemo = true;

                if (tipCloveka.getSelectedItem().toString().equals("")) {
                    idemo = false;
                    tipCloveka.setBackgroundColor(Color.RED);
                } else {
                    tipCloveka.setBackgroundColor(Color.WHITE);
                }

                if (ime.getText().toString().equals("")) {
                    idemo = false;
                    ime.setBackgroundColor(Color.RED);
                } else {
                    ime.setBackgroundColor(Color.WHITE);
                }

                if (priimek.getText().toString().equals("")) {
                    idemo = false;
                    priimek.setBackgroundColor(Color.RED);
                } else {
                    priimek.setBackgroundColor(Color.WHITE);
                }

                if (idemo) {
                    WasserApplication.ime = ime.getText().toString();
                    WasserApplication.priimek = priimek.getText().toString();
                    WasserApplication.tipCloveka = tipCloveka.getSelectedItem().toString();

                    Intent i = new Intent(getApplicationContext(), SetupWeightActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
}
