package si.d0.wasser;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

public class SetupWeightActivity extends AppCompatActivity {
    boolean jeAktiven = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_weight);

        final EditText teza = (EditText) findViewById(R.id.editTextTeza);

        final Switch aktiven = (Switch) findViewById(R.id.aktivenSwitch);
        aktiven.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    aktiven.setText("Aktiven");
                } else {
                    aktiven.setText("Neaktiven");
                }
                jeAktiven = isChecked;
            }
        });
        aktiven.setChecked(false);

        Button naprej = (Button)findViewById(R.id.setupWeightNextButton);
        naprej.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean idemo = true;

                if (teza.getText().toString().equals("")) {
                    idemo = false;
                    teza.setBackgroundColor(Color.RED);
                } else {
                    teza.setBackgroundColor(Color.WHITE);
                }

                if (idemo) {
                    WasserApplication.teza = Integer.parseInt(teza.getText().toString());
                    WasserApplication.aktiven = jeAktiven;
                    WasserApplication.prijavljen = true;

                    int potrebnoVode = 0;
                    if (WasserApplication.tipCloveka.equals("Moski")) {
                        if (WasserApplication.aktiven) {
                            potrebnoVode = 40 * WasserApplication.teza;
                        } else {
                            potrebnoVode = 35 * WasserApplication.teza;
                        }
                    } else if (WasserApplication.tipCloveka.equals("Zenska")) {
                        if (WasserApplication.aktiven) {
                            potrebnoVode = 35 * WasserApplication.teza;
                        } else {
                            potrebnoVode = 30 * WasserApplication.teza;
                        }
                    } else {
                        if (WasserApplication.aktiven) {
                            potrebnoVode = 30 * WasserApplication.teza;
                        } else {
                            potrebnoVode = 25 * WasserApplication.teza;
                        }
                    }
                    WasserApplication.potrebnoPopitiVode = potrebnoVode;

                    WasserApplication.shrani();

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
}
