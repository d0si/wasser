package si.d0.wasser;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView nameText;
    ProgressBar bar;
    Button dodajVodoButton;
    TextView popiteVodeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!WasserApplication.prijavljen) {
            Intent i = new Intent(this, SetupNameActivity.class);
            startActivity(i);
            finish();
        } else {
            bar = (ProgressBar) findViewById(R.id.progressBar);
            nameText = (TextView) findViewById(R.id.textViewIme);
            dodajVodoButton = (Button) findViewById(R.id.dodajButton);
            popiteVodeText = (TextView) findViewById(R.id.popiteVodeText);

            nameText.setText(WasserApplication.ime + " " + WasserApplication.priimek + " (" + WasserApplication.teza + ")");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                bar.setMin(0);
            }

            bar.setMax(WasserApplication.potrebnoPopitiVode);
            bar.setProgress(WasserApplication.kolicinaPopiteVode);

            float procentPopiteVode = WasserApplication.kolicinaPopiteVode / WasserApplication.potrebnoPopitiVode;
            if (procentPopiteVode < 0.5) {
                bar.getProgressDrawable().setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (procentPopiteVode < 0.9) {
                bar.getProgressDrawable().setColorFilter(Color.YELLOW, android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                bar.getProgressDrawable().setColorFilter(Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN);
            }

            popiteVodeText.setText("Popite vode: " + WasserApplication.kolicinaPopiteVode + "ml od " + WasserApplication.potrebnoPopitiVode + "ml");

            dodajVodoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(), AddWaterActivity.class);
                    startActivity(i);
                    finish();
                }
            });
        }
    }
}
