package si.d0.wasser;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AddWaterActivity extends AppCompatActivity {

    Button dodajButton;
    TextView kolicinaVodeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_water);

        dodajButton = (Button) findViewById(R.id.dodajButton);
        kolicinaVodeText = (TextView) findViewById(R.id.kolicinaVodeEditText);

        dodajButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean idemo = true;

                if (kolicinaVodeText.getText().toString().equals("")) {
                    idemo = false;
                    kolicinaVodeText.setBackgroundColor(Color.RED);
                } else {
                    kolicinaVodeText.setBackgroundColor(Color.WHITE);
                }

                if (idemo) {
                    WasserApplication.kolicinaPopiteVode += Integer.parseInt(kolicinaVodeText.getText().toString());

                    WasserApplication.shrani();

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
}
